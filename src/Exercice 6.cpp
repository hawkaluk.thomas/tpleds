// main.cpp
// Hawkaluk Thomas
#include <Arduino.h>
//Pour adapter la lib FastLED à l'ESP
#define FASTLED_ESP8266_RAW_PIN_ORDER
//ajout de la lib
#include <FastLED.h>
#define NUM_LEDS 10 //nombre de LED du ruban
#define DATA_PIN D4 //D4 broche du bus de commande du ruban
//Création d'un tableau pour stocker et contrôler les LEDS
CRGB leds[NUM_LEDS];
int i = 0;
float color = 0;
void setup()
{
    //configuration du ruban de LEDs
    FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}

//Exercice 4)
void loop()
{
    leds[i%NUM_LEDS] = CHSV(color, 200, 200);
    i++;            // pour avancer d'une Led
    FastLED.show(); // Affichage des Leds
    color = color+22;
    delay(5); // pour changer la vitesse de l'affichage des leds
    if (color >= 255)
    {
        color = 0;
    }
    

}
