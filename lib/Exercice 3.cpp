// main.cpp
// Hawkaluk Thomas
#include <Arduino.h>
//Pour adapter la lib FastLED à l'ESP
#define FASTLED_ESP8266_RAW_PIN_ORDER
//ajout de la lib
#include <FastLED.h>
#define NUM_LEDS 10 //nombre de LED du ruban
#define DATA_PIN D4 //D4 broche du bus de commande du ruban
//Création d'un tableau pour stocker et contrôler les LEDS
CRGB leds[NUM_LEDS];
int i = 0;
int e = 1;
int teinte = 0;
int sat = 0;
void setup()
{
    //configuration du ruban de LEDs
    FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}
//Exercice 3)
void loop()
{
    FastLED.clear();                                // Réinitialisation des Leds

    leds[i] = CHSV(teinte, sat, 255);               // La couleur de la Led
    i = i + e;                                      // pour avancer d'une Led
    FastLED.show();                                 // Affichage des Leds
    delay(50);                                     // pour changer la vitesse de l'affichage des leds

    FastLED.clear();
    if (NUM_LEDS -2 == i - 1 || i == 0)             // si le numéro de la Led est le même que i alors il s'active
    {
        e = e * -1;                                 // pour revenir en arrière
    }
    if (teinte <= 250 && sat <= 150 ) {             // si la teinte et la saturation dépassent leurs limites
        teinte = teinte + 25;
        sat = sat + 15;
    }
    else                                            // sinon elle recommence à zéro
    {
        teinte = 0;                                 // Réinitialisation de la teinte à 0
        sat = 0;                                    // Réinitialisation de la saturation à 0
    }
    
    
    


}
