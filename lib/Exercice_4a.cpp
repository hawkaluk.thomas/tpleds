// main.cpp
// Hawkaluk Thomas
#include <Arduino.h>
//Pour adapter la lib FastLED à l'ESP
#define FASTLED_ESP8266_RAW_PIN_ORDER
//ajout de la lib
#include <FastLED.h>
#define NUM_LEDS 10 //nombre de LED du ruban
#define DATA_PIN D4 //D4 broche du bus de commande du ruban
//Création d'un tableau pour stocker et contrôler les LEDS
CRGB leds[NUM_LEDS];
int i = 0;
int i2= 1;
int i3= 2;
int e = 1;
void setup()
{
    //configuration du ruban de LEDs
    FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}

void allumerLeds(int prmNumPrincipal){
    leds[i] = CHSV(150, 150, 50);
    leds[i2] = CHSV(150, 150, 200);
    leds[i3]  = CHSV(150, 150, 50);
    i = i + e;
    i2 = i2 + e;
    i3 = i3 + e;                                      // pour avancer d'une Led
    if (NUM_LEDS -1 == i - 1 || i == 0)             // si le numéro de la Led est le même que i alors il s'active
    {
        i = 0;                                 // pour revenir en arrière

    }
    if (NUM_LEDS -1 == i2 - 1 || i2 == 0)             // si le numéro de la Led est le même que i alors il s'active
    {
        i2 = 0;                                 // pour revenir en arrière
    }
    if (NUM_LEDS -1 == i3 - 1 || i3 == 0)             // si le numéro de la Led est le même que i alors il s'active
    {
        i3 = 0;                                 // pour revenir en arrière
    }
}




//Exercice 4a)
void loop()
{
    allumerLeds(i);
    FastLED.show();                                 // Affichage des Leds
    delay(500);                                     // pour changer la vitesse de l'affichage des leds
    FastLED.clear();
}
