// main.cpp
// Hawkaluk Thomas
#include <Arduino.h>
//Pour adapter la lib FastLED à l'ESP
#define FASTLED_ESP8266_RAW_PIN_ORDER
//ajout de la lib
#include <FastLED.h>
#define NUM_LEDS 10 //nombre de LED du ruban
#define DATA_PIN D4 //D4 broche du bus de commande du ruban
//Création d'un tableau pour stocker et contrôler les LEDS
CRGB leds[NUM_LEDS];
int e = 1;
void setup()
{
    //configuration du ruban de LEDs
    FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}

void allumerLeds(int prmNumPrincipal)
{
    prmNumPrincipal = prmNumPrincipal + e;
    leds[(prmNumPrincipal - 1) % NUM_LEDS] = CHSV(150, 150, 50);
    leds[prmNumPrincipal % NUM_LEDS] = CHSV(150, 150, 200);
    leds[(prmNumPrincipal + 1) % NUM_LEDS] = CHSV(150, 150, 50);
    e++;            // pour avancer d'une Led
    FastLED.show(); // Affichage des Leds
    FastLED.clear();
}

//Exercice 4)
void loop()
{
    allumerLeds(4);
    delay(500); // pour changer la vitesse de l'affichage des leds
}
